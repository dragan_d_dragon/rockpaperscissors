import { PlayerList } from '../state/PlayerList';
import { PlayerType } from '../state/Player';




export const playerList = new PlayerList([{
    id: 0,
    name: 'John',
    type: PlayerType.HUMAN,
    score: 0,
    activeWeapon: null,
  },
  {
    id: 0,
    name: 'Skynet',
    type: PlayerType.CPU,
    score: 0,
    activeWeapon: null,
  }
]);

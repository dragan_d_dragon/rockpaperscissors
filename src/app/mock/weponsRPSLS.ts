import { WeaponSet } from '../state/WeaponSet';
import { Weapon } from '../state/Weapon';

enum PRSLSWeapons {
  PAPER = 'PAPER',
  ROCK = 'ROCK',
  SCISSORS = 'SCISSORS',
  LIZARD = 'LIZARD',
  SPOCK = 'SPOCK',
}

const wpns = [
  new Weapon(
    PRSLSWeapons.PAPER,
    null,
    [ PRSLSWeapons.ROCK, PRSLSWeapons.SPOCK ]
  ),
  new Weapon(
    PRSLSWeapons.ROCK,
    null,
    [ PRSLSWeapons.SCISSORS, PRSLSWeapons.LIZARD ]
  ),
  new Weapon(
    PRSLSWeapons.SCISSORS,
    null,
    [ PRSLSWeapons.PAPER, PRSLSWeapons.LIZARD]
    ),
    new Weapon(
      PRSLSWeapons.LIZARD,
      null,
      [ PRSLSWeapons.PAPER, PRSLSWeapons.SPOCK]
    ),
    new Weapon(
      PRSLSWeapons.SPOCK,
      null,
      [ PRSLSWeapons.PAPER, PRSLSWeapons.ROCK]
    )
];

export const pRSLSSet = new WeaponSet(wpns);



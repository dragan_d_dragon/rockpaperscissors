import { WeaponSet } from '../state/WeaponSet';
import { Weapon } from '../state/Weapon';

class PRSWeapons {
  static readonly PAPER = 'PAPER';
  static readonly ROCK = 'ROCK';
  static readonly SCISSORS = 'SCISSORS';
  private constructor () {}
}

const wpns = [
  new Weapon(
    PRSWeapons.PAPER,
    null,
    [ PRSWeapons.ROCK ]
  ),
  new Weapon(
    PRSWeapons.ROCK,
    null,
    [ PRSWeapons.SCISSORS ]
  ),
  new Weapon(
    PRSWeapons.SCISSORS,
    null,
    [ PRSWeapons.PAPER ]
  )
];

export const pRSSet = new WeaponSet(wpns);



import { Component, OnInit, OnDestroy } from '@angular/core';
import { StateSubscriber } from './state/StateSubscriber';
import { GameStateService } from './state/gameState.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent extends StateSubscriber implements OnInit, OnDestroy {
  private _title = 'rock paper scissors';
  private _gameStarted = false;
  constructor (gameStateService: GameStateService) { super(gameStateService); }

  protected _updateLocal = state => this._gameStarted = state.gameStarted;
  protected _updateState = gameStateService => null;

  ngOnInit () { this._subscribe(); }
  ngOnDestroy () { this._unsubscribe(); }
}



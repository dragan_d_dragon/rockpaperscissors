import { Component, OnInit, OnDestroy } from '@angular/core';
import { GameStateService } from '../../state/gameState.service';
import { Player, PlayerType } from '../../state/Player';
import { StateSubscriber } from '../../state/StateSubscriber';
import { Weapon } from '../../state/Weapon';
import { Round } from '../../state/Round';
import { WeaponSet } from '../../state/WeaponSet';
import { PlayerService } from '../../service/player.service';
import { WeaponService } from '../../service/weapon.service';
import { GameService } from '../../service/game.service';
import { WeaponSetType } from '../../state/WeaponSetType';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  readonly SELECT_WEAPON = Round.SELECT_WEAPON;
  readonly DECIDE_WINNER = Round.DECIDE_WINNER;
  readonly ROUND_END = Round.ROUND_END;

  private _players: Array<Player>;
  private _weapons: Array<Weapon>;
  private _rounds: number;
  private _playedRounds: number;

  private _roundPosition: Round;
  private _selectedPlayer: Player;
  private _winner: Player | 0;

  private _gameIterator: Generator;
  private _roundIterator: Generator;
  private _slectWeaponIterator: Generator;

  private _gameEnd = false;

  private *_gameFlow () {
    while (this._playedRounds < this._rounds) {
      this._roundIterator = this._roundFlow(this._gameIterator);
      window.setTimeout(this._roundIterator.next.bind(this), 0);
      this._updateState();
      this._save();
      yield this._playedRounds += 1;
    }
    this._gameEnd = true;
  }

  private *_roundFlow (done: Generator) {
      this._slectWeaponIterator = this._selecWeaponFlow(this._roundIterator);
      window.setTimeout(this._slectWeaponIterator.next.bind(this), 0);
    yield this._roundPosition = Round.SELECT_WEAPON;
      window.setTimeout(this._decideRoundWinner.bind(this), 0);
    yield this._roundPosition = Round.DECIDE_WINNER;
    yield this._roundPosition = Round.ROUND_END;
    yield done.next();
  }

  private *_selecWeaponFlow (done: Generator) {
    const l = this._players.length;
    let i = 0;
    while (i < l) {
      this._selectedPlayer = this._players[i];
      if (this._selectedPlayer.type === PlayerType.CPU) {
        window.setTimeout(this._selectWeaponAI.bind(this), 0);
      }
      yield i++;
    }
    done.next();
  }

  constructor (
    private playerService: PlayerService,
    private weaponService: WeaponService,
    private gameService: GameService
  ) { }

  ngOnInit () {
    this._load();
    this.weaponService.changeSet(this.gameService.weaponSet);
    this._updateLocal();
    this._initGameFow();
  }

  private _initGameFow () {
    this._gameIterator = this._gameFlow();
    this._gameIterator.next();
  }

  private _updateLocal () {
    this._players = this.playerService.getPlayers();
    this._weapons = this.weaponService.getWeapons();
    this._rounds = this.gameService.rounds;
    this._playedRounds = this.gameService.playedRounds;

  }

  private _updateState () {
    this.playerService.updatePlayer(this._players[0]);
    this.playerService.updatePlayer(this._players[1]);
    this.gameService.playedRounds =  this._playedRounds;
  }

  private _save () {
    this.playerService.save();
    this.gameService.save();
  }

  private _load () {
    this.playerService.load();
    this.gameService.load();
  }

  private _selectWeaponAI () {
    const weapon: Weapon = this._weapons[Math.floor(Math.random() * (this._weapons.length - 0.000000000000001))];
    this._selectWeapon(weapon);
  }

  /**
   * Calls next on _selectWeaponIterator.
   * Make sure that iterator exists and call async manner if iterator is running.
   */
  private _selectWeapon (weapon: Weapon) {
    this._selectedPlayer.activeWeapon = weapon;
    this._slectWeaponIterator.next();
  }

  /**
   * Calls next on _roundIterator.
   * Make sure that iterator exists and call async manner if iterator is running.
   */
  private _decideRoundWinner () {
    const winnerScale: number = this.weaponService.getStronger(this._players[0].activeWeapon, this._players[1].activeWeapon);
    switch (winnerScale) {
      case -1:
        this._winner = this._players[0];
        this._winner.score += 1;
        break;
      case 0: this._winner = 0;
        break;
      case 1:
        this._winner = this._players[1];
        this._winner.score += 1;
        break;
    }
    this._roundIterator.next();
  }

  private _nextRound () {
    if (this._roundPosition === Round.ROUND_END) {
      this._gameIterator.next();
    }
  }

  private _resetGame() {
    this._players.forEach(p => p.score = 0);
    this._playedRounds = 0;
    this._gameEnd = false;
    this._initGameFow();
  }
}

import { Component, OnInit, Input, OnDestroy, Inject } from '@angular/core';
import { Player, PlayerType } from '../../state/Player';
import { PlayerService } from '../../service/player.service';
import { GameService } from '../../service/game.service';
import { Router } from '@angular/router';
import { WeaponSetType } from '../../state/WeaponSetType';
import { WeaponService } from '../../service/weapon.service';
import { Action } from '../../../../node_modules/rxjs/internal/scheduler/Action';


@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  private _players: Array<Player> = [];
  private _playerTypes: Array<string> = Object.keys(PlayerType);
  private _rounds = 0;
  private _activeWeaponSet: WeaponSetType;

  private _fieldsValid = false;
  private _weponSetButtons: Array<{name: string, value: WeaponSetType, active: boolean}>;

  private _weaponSetChange = (set: WeaponSetType) => {
    this.weaponService.changeSet(set);
    this._activeWeaponSet = set;
    this._updateState();
    this._weponSetButtons = Object.keys(WeaponSetType).map(k => ({
      name: WeaponSetType[k],
      value: WeaponSetType[k],
      active: WeaponSetType[k] === this._activeWeaponSet,
    }));
  }

  constructor (
    private playerService: PlayerService,
    private gameService: GameService,
    private weaponService: WeaponService,
    private router: Router
  ) {}

  ngOnInit () {
    this.playerService.clearAll();

    this._players = this.playerService.getPlayers();
    this._players.forEach(p => p.score = 0);
    this.gameService.playedRounds = 0;
    this._weaponSetChange(WeaponSetType.PRS);
  }

  private _updateState = () => {
    this.playerService.updatePlayer(this._players[0]);
    this.playerService.updatePlayer(this._players[1]);
    this.gameService.rounds = this._rounds;
    this.gameService.weaponSet = this._activeWeaponSet;
  }

  private _validate (): boolean {
    return this._fieldsValid = (
      this._players.every(p => p.name.length > 0) &&
      this._rounds > 0
    );
  }

  private _onChange () {
      if (this._validate()) {
        this._updateState();
      }
  }
}

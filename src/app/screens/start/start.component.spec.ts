import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartComponent } from './start.component';
import { FormsModule } from '../../../../node_modules/@angular/forms';
import { AppRoutingModule } from '../../app-routing.module';
import { GameComponent } from '../game/game.component';
import { RoundCountComponent } from '../../components/round-count/round-count.component';
import { PlayerComponent } from '../../components/player/player.component';
import { WeaponComponent } from '../../components/weapon/weapon.component';
import { APP_BASE_HREF } from '../../../../node_modules/@angular/common';

describe('StartComponent', () => {
  let component: StartComponent;
  let fixture: ComponentFixture<StartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        StartComponent,
        GameComponent,
        RoundCountComponent,
        PlayerComponent,
        WeaponComponent,
      ],
      imports: [ AppRoutingModule, FormsModule ],
      providers: [
        { provide: APP_BASE_HREF, useValue : '/' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

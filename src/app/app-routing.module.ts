import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './screens/game/game.component';
import { StartComponent } from './screens/start/start.component';

const routes: Routes = [
  { path: '', redirectTo: '/new', pathMatch: 'full' },
  {path: 'new', component: StartComponent},
  {path: 'play', component: GameComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }

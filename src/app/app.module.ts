import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { StartComponent } from './screens/start/start.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GameComponent } from './screens/game/game.component';
import { PlayerComponent } from './components/player/player.component';
import { RoundCountComponent } from './components/round-count/round-count.component';
import { WeaponComponent } from './components/weapon/weapon.component';
import { AppRoutingModule } from './/app-routing.module';
import { RadioButtonsComponent } from './components/radio-buttons/radio-buttons.component';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    GameComponent,
    PlayerComponent,
    RoundCountComponent,
    WeaponComponent,
    RadioButtonsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

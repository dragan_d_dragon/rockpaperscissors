import { Injectable } from '@angular/core';
import { playerList } from '../mock/playerList';
import { Player } from '../state/Player';
import { Storable } from '../state/Storable';
import { PlayerList } from '../state/PlayerList';

@Injectable({
  providedIn: 'root'
})
export class PlayerService extends Storable {

  private _playerList = playerList;
  protected _name = 'players';

  constructor() { super(); }

  protected _getContext () {
    return JSON.stringify(this._playerList.getPlayerList());
  }

  protected _loadContext (context) {
    this._playerList = new PlayerList(JSON.parse(context));
  }

  getPlayers () {
    return this._playerList.getPlayerList();
  }

  updatePlayer (player: Player) {
    this._playerList.updatePlayer(player);
  }
}

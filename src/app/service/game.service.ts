import { Injectable } from '@angular/core';
import { Game } from '../state/Game';
import { Storable } from '../state/Storable';
import { WeaponSetType } from '../state/WeaponSetType';

@Injectable({
  providedIn: 'root'
})
export class GameService extends Storable {

  private _game = new Game();
  protected _name = 'game';

  constructor() { super(); }

  protected _getContext (): string {
    return JSON.stringify(this._game);
  }

  protected _loadContext (context) {
    // TODO: try-catch.
    this._game = JSON.parse(context);
  }

  get rounds () { return this._game.rounds; }
  set rounds (rounds: number) { this._game.rounds = rounds; }
  get playedRounds () { return this._game.playedRounds; }
  set playedRounds (playedRounds: number) {this._game.playedRounds = playedRounds; }
  get weaponSet () { return this._game.weaponSet; }
  set weaponSet (set: WeaponSetType) { this._game.weaponSet = set; }
}

import { Injectable } from '@angular/core';
import { pRSSet } from '../mock/weapons';
import { WeaponSet } from '../state/WeaponSet';
import { Weapon } from '../state/Weapon';
import { WeaponSetType } from '../state/WeaponSetType';
import { pRSLSSet } from '../mock/weponsRPSLS';

@Injectable({
  providedIn: 'root'
})
export class WeaponService {

  private _activeSet: WeaponSetType = WeaponSetType.PRS;
  private _weaponSet: WeaponSet = pRSSet ;

  constructor() { }

  getWeapons (): Weapon[] { return this._weaponSet.getWeapons(); }
  arm() { return this._weaponSet.arm(); }
  getStronger (w1: Weapon, w2: Weapon): number { return this._weaponSet.getStronger(w1, w2); }

  changeSet (set: WeaponSetType) {
    switch (set) {
      case WeaponSetType.PRS:
        this._weaponSet = pRSSet;
        break;
      case WeaponSetType.PRLS:
        this._weaponSet = pRSLSSet;
        break;
    }
  }
}

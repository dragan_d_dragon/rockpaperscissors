import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeaponComponent } from './weapon.component';
import { pRSSet } from '../../mock/weapons';
import { Component, ViewChild } from '../../../../node_modules/@angular/core';

describe('WeaponComponent', () => {
  let component: WeaponComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeaponComponent, TestHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance.weaponComponent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  @Component({
    selector: 'app-host-component',
    template: '<app-weapon [weapon]="weapon"></app-weapon>'
  })
  class TestHostComponent {
    @ViewChild(WeaponComponent)
    public weaponComponent: WeaponComponent;

    public weapon = pRSSet.getWeapons()[0];
  }
});

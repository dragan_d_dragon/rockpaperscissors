import { Component, OnInit, Input } from '@angular/core';
import { Weapon } from '../../state/Weapon';

@Component({
  selector: 'app-weapon',
  templateUrl: './weapon.component.html',
  styleUrls: ['./weapon.component.css']
})
export class WeaponComponent implements OnInit {
  @Input() weapon: Weapon;

  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundCountComponent } from './round-count.component';

describe('RoundCountComponent', () => {
  let component: RoundCountComponent;
  let fixture: ComponentFixture<RoundCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoundCountComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

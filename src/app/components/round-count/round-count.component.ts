import { Component, OnInit, Inject, Input } from '@angular/core';

@Component({
  selector: 'app-round-count',
  templateUrl: './round-count.component.html',
  styleUrls: ['./round-count.component.css']
})
export class RoundCountComponent implements OnInit {

  @Input() rounds: number;
  @Input() roundsPlayed: number;

  constructor() { }

  ngOnInit() {
  }

}

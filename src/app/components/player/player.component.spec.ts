import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerComponent } from './player.component';
import { ViewChild, Component } from '@angular/core';
import { playerList } from '../../mock/playerList';

describe('PlayerComponent', () => {
  let component: PlayerComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerComponent, TestHostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance.playerComponent;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  @Component({
    selector: 'app-host-component',
    template: '<app-player [player]="player"></app-player>'
  })
  class TestHostComponent {
    @ViewChild(PlayerComponent)
    public playerComponent: PlayerComponent;

    public player = playerList.getPlayerList()[0];
  }
});

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-radio-buttons',
  templateUrl: './radio-buttons.component.html',
  styleUrls: ['./radio-buttons.component.css']
})
export class RadioButtonsComponent {

  @Input() valueChange: (any) => void;
  @Input() buttons: Array<{value: any, name: string, active: true}>;
  @Input() legend: string;

  constructor() { }
}

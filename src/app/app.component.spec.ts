import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Router } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { StartComponent } from './screens/start/start.component';
import { GameComponent } from './screens/game/game.component';
import { FormsModule } from '@angular/forms';
import { RoundCountComponent } from './components/round-count/round-count.component';
import { WeaponComponent } from './components/weapon/weapon.component';
import { PlayerComponent } from './components/player/player.component';
import { APP_BASE_HREF } from '@angular/common';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        StartComponent,
        GameComponent,
        PlayerComponent,
        RoundCountComponent,
        WeaponComponent,
      ],
      imports: [ AppRoutingModule, FormsModule ],
      providers: [
        { provide: APP_BASE_HREF, useValue : '/' }
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  // it(`should have as title 'rockPaperScissors'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('rockPaperScissors');
  // }));
  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to rockPaperScissors!');
  // }));
});

import { Weapon } from './Weapon';

export class WeaponSet {
  private _weapons: Array<Weapon> = [];
  private _armed: boolean;

  constructor (initData?: Array<Weapon>) {
    if (initData) {
      initData.forEach(w => this._add(w));
    }
  }

  get armed () { return this._armed; }

  arm () {
    const winRelations = 0 +
      (Math.pow(this._weapons.length, 2) - this._weapons.length) / 2 / this._weapons.length;
    this._weapons.forEach(w => {
      if (w.kills.length !== winRelations) {
        throw new Error('Wepons not armed properly.');
      }
    });
    this._armed = true;
  }

  unarm () { this._armed = false; }

  private _armCheck () {
    if (this._armed ) {
      throw new Error('Weapons armed, can\'t move them now.');
    }
  }

  private _findWeaponIndex (name: string): number {
    return this._weapons.findIndex(w => w.name.toLowerCase() === name.toLowerCase());
  }

  isNameUnique (name): boolean {
    return -1 === this._findWeaponIndex(name);
  }

  private _add (weapon: Weapon) {
    this._armCheck();
    if (this.isNameUnique(weapon.name)) {
      this._weapons.push(weapon);
    } else {
      throw new Error(`Can not add weapon, name "${weapon.name}" is not unique.`);
    }
  }

  private _remove (nameOrWeapon: string | Weapon): boolean {
    this._armCheck();
    const index = typeof nameOrWeapon === 'string' ?
      this._findWeaponIndex(nameOrWeapon) :
      this._findWeaponIndex(nameOrWeapon.name);
    if (index !== -1) { this._weapons.splice(index, 1); }
    return index !== -1;
  }

  get (name: string): Weapon | null {
    const index = this._findWeaponIndex(name);
    return index !== -1 ? {...this._weapons[index]} : null;
  }

  getWeapons (): Array<Weapon> {
    return  this._weapons.map(w => new Weapon(w.name, w.image, w.kills));
  }

  /**
   * Calculates which weapon is stronger.
   * @param w1 {Weapon} Weapon1
   * @param w2 {Weapon} Weapon2
   * @returns {number} One values -1, 0, 1, for "w1 wins", "draw", "w2w wins", respectively.
   */
  getStronger(w1: Weapon, w2: Weapon): number {
    if (!this.isNameUnique(w1.name) && !this.isNameUnique(w2.name)) {
      if (w1.name === w2.name) {
        return 0;
      }
      if (w1.kills.find(w => w === w2.name)) {
       return -1;
      }
      return 1;
    }
  }
}

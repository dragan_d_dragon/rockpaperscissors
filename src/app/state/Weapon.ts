export class Weapon {
  name: string;
  image: string | null;
  kills: Array<string>;

  constructor (name, image, kills) {
    this.name = name;
    this.image = image;
    this.kills = kills;
  }
}


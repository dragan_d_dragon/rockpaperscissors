import { WeaponSetType } from './WeaponSetType';

export class Game {
  rounds = 0;
  playedRounds = 0;
  weaponSet: WeaponSetType;
}

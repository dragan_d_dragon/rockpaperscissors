import { Subscription } from '../../../node_modules/rxjs';
import { State } from './State';
import { GameStateService } from './gameState.service';

export abstract class StateSubscriber {
  private _subscription: Subscription;

  constructor(private _gameStateService: GameStateService) {}

  protected abstract _updateState: (gameStateService: GameStateService) => void;
  protected abstract _updateLocal: (state: State) => void;

  /**
   * Subcrbes watcher function on state.
   * You probbably want to call this onInit.
   */
  protected _subscribe () {
    this._subscription = this._gameStateService.subscribe(this._updateLocal);
  }

  /**
   * Unsubscribes watcher function.
   * You should unsubscribe before destroying component (onDestroy).
   */
  protected _unsubscribe() {
    if (this._subscription) {
      this._subscription.unsubscribe();
    } else {
      throw new Error('Can not unsubscribe on non-existing subscritption.');
    }
  }

  protected _pushState () {
    this._unsubscribe();
    this._updateState(this._gameStateService);
    this._subscribe();
  }
}

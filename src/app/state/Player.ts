import { Weapon } from './Weapon';

export enum PlayerType {
  CPU = 'CPU',
  HUMAN = 'HUMAN',
}

export type PlayerTypeType = PlayerType.CPU | PlayerType.HUMAN;

export class Player {
  id: number;
  name: string;
  type: PlayerTypeType;
  score =  0;
  activeWeapon: Weapon | null;
}

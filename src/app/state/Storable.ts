export abstract class Storable {

  protected abstract _name: string;

  protected abstract _getContext (): string;
  protected abstract _loadContext (context: string): void;

  save () {
    window.localStorage.setItem(this._name, this._getContext());
  }

  load (): boolean {
    const context = window.localStorage.getItem(this._name);
    if (context) {
      this._loadContext(context);
    }
    return !!context;
  }

  clear () {
    window.localStorage.removeItem(this._name);
  }

  clearAll () {
    window.localStorage.clear();
  }
}

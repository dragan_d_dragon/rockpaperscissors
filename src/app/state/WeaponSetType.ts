export enum WeaponSetType {
  PRS = 'paper-rock-scissors',
  PRLS = 'paper-rock-lizard-spock',
}

import { Player } from './Player';

export class PlayerList {
  private _playerList: Array<Player> = [];
  private _id = 1;

  constructor (playerArray?: Array<Player>) {
    if (playerArray) {
      playerArray.forEach(p => this.addPlayer(p));
    }
  }

  private _findPlayer (id): Player | undefined {
    return this._playerList.find(p => id === p.id);
  }

  addPlayer (player: Player): void {
    this._playerList.push({...player, id: this._id++});
  }

  /**
   * Retrives copy of player
   * @returns Copy of player
  */
  getPlayer (id: number): Player | void {
    const player = this._findPlayer(id);
    return {...player};
  }

  updatePlayer (player: Player): boolean {
    console.log('UPDATIN p', player);

    const index = this._playerList.findIndex(p => p.id === player.id);
    if (index !== -1) {
      console.log('UPDATIN PLAYER', player);
      this._playerList[index] = {...player};
    }
    return index !== -1;
  }

  /**
   * Makes copy of player-list.
   * @returns {Array<Player>} Copy of player-list.
   */
  getPlayerList (): Array<Player> {
    return this._playerList.map(p => ({...p}));
  }

  get length (): number {
    return this._playerList.length;
  }
}

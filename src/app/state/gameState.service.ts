import { Injectable } from '@angular/core';
import { Player } from './Player';
import { BehaviorSubject, Subscription } from 'rxjs';
import { playerList } from '../mock/playerList';
import { State } from './State';
import { pRSSet } from '../mock/weapons';

@Injectable({
  providedIn: 'root'
})
export class GameStateService {

  private _stateSubject: BehaviorSubject<State>;

  constructor() {
    this._stateSubject = new BehaviorSubject(new State(playerList, pRSSet));
  }
  private get _state(): State {
    return this._stateSubject.getValue();
  }

  updatePlayer(player: Player): boolean {
    const udpated = this._state.players.updatePlayer(player);
    if (udpated) {
      this._stateSubject.next(this._state);
    }
    return udpated;
  }

  private _setProp (prop: string, value: string | number | boolean | null) {
    if (
      this._state[prop] !== value &&
      (
        this._state[prop] === this._state[prop] ||
        value === value
      )
    ) {
      this._state[prop] = value;
      this._stateSubject.next(this._state);
    }
  }

  setGameStarted(gameStarted: boolean) { this._setProp('gameStarted', gameStarted); }
  setRounds (rounds: number) { this._setProp('rounds', rounds); }
  setRoundsPlayed (rounds: number) { this._setProp('roundsPlayed', rounds); }
  setRoundPosition (roundPosition: number | null) { this._setProp('roundPosition', roundPosition); }
  setSelectedPlayerIndex (index: number | null) { this._setProp('selectedPlayerIndex', index); }

  // TODO: provide copies/immutable data instead of state reference.
  // (Traverse state and define getters/setters for each prop?).
  subscribe(subscriber: (State) => void): Subscription {
    return this._stateSubject.subscribe(subscriber);
  }
}

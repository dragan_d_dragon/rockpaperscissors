import { PlayerList } from './PlayerList';
import { Round } from './Round';
import { WeaponSet } from './WeaponSet';

export class State {
  players: PlayerList;
  weapons: WeaponSet;
  rounds = 0;
  roundsPlayed = 0;
  private _gameStarted = false;
  roundPosition: Round | null = null;
  selectedPlayerIndex: number | null = null;
  constructor (playerList: PlayerList, weaponSet: WeaponSet) {
    this.players = playerList;
    this.weapons = weaponSet;
  }

  get gameStarted (): boolean {
    return this._gameStarted;
  }
  set gameStarted (start: boolean) {
    this.weapons.arm();
    if (start) {
      if (
        this.players.length > 0  &&
        this.rounds > 0 &&
        this.weapons.armed
      ) {
        this.roundsPlayed = 0;
        this._gameStarted = start;
      } else {
        throw new Error('Can not start game, state not initalized properly.');
      }
    } else {
      this._gameStarted = start;
    }
  }
}

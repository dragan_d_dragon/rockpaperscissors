export enum Round {
  SELECT_WEAPON = 0,
  DECIDE_WINNER = 1,
  ROUND_END = 2,
}
